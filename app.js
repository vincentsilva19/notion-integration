const express = require('express')
const app = express()
const port = 3000
const notionSecret = 'secret_WTAOw129l61J3HSFFekgedBzHNZ2rW7RliNHswEX1C1'
const { Client } = require("@notionhq/client")

const notion = new Client({
    auth: notionSecret
})


app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.post('/issues', (req, res) => {
    (async () => {
        // const listUsersResponse = await notion.users.list({})
        console.log(req.body)
        res.send('ok')
    })()
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})